package es.ua.eps.phoneinfo;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView details;
    TelephonyManager tm;
    GsmCellLocation cellLocation;

    private static final int PHONE_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        details = (TextView) findViewById(R.id.details);
        details.setText("");

        tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_COARSE_LOCATION}, PHONE_PERMISSION);
        } else {
            getInfo();
        }
    }

    private void getInfo() {

        cellLocation = (GsmCellLocation) tm.getCellLocation();

        String simOperator = tm.getSimOperatorName();
        String simSerialNumber = tm.getSimSerialNumber();
        String networkOperator = tm.getNetworkOperatorName();
        String carrierCountry = tm.getNetworkCountryIso();
        String lineNumber = tm.getLine1Number();
        String imei = tm.getDeviceId();
        String imsi = tm.getSubscriberId();
        int networkType = tm.getNetworkType();

        String cellCid = String.valueOf(cellLocation.getCid());
        String cellLac = String.valueOf(cellLocation.getLac());

        String text = String.format("SIM operator: %s \n"
                        + "SIM Serial number: %s\n"
                        + "Network operator: %s\n"
                        + "Carrier country code: %s\n"
                        + "Network type: %s\n"
                        + "IMSI: %s\n"
                        + "IMEI: %s\n"
                        + "\n\n"
                        + "Cell Info: \n"
                        + "Cid: %s\n"
                        + "Lac: %s",
                simOperator,
                simSerialNumber,
                networkOperator,
                carrierCountry,
                getNetworkClass(networkType),
                imsi,
                imei,
                cellCid,
                cellLac
        );

        details.setText(text);
    }

    public String getNetworkClass(int networkType) {
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "2G";
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "3G";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "4G";
            default:
                return "Unknown";
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PHONE_PERMISSION:
                if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    getInfo();
                }
                break;
        }
    }
}
